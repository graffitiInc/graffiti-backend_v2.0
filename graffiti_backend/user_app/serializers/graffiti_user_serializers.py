from django.conf import settings
from rest_framework import serializers
from user_app.models import GraffitiUser

class GraffitiUserCreateSerializer(serializers.ModelSerializer):
    #user_id = serializers.SerializerMethodField(get_user_id)
    class Meta:
        model = GraffitiUser
        fields = ('username',
                  'email',
                  'password',
                  'created_at_utc',
                  'profile_image'
        )

    def create(self, validated_data):
        #graffiti_user = GraffitiUser(**validated_data)
        #graffiti_user.save()
        password = validated_data.pop('password', None)
        graffiti_user = self.Meta.model(**validated_data)
        if password is not None:
            graffiti_user.set_password(password)
            graffiti_user.save()
            return graffiti_user

    #def get_user_id(self, object):
    #    return object.id

class GraffitiUserDetailSerializer(serializers.ModelSerializer):
    #user_id = serializers.SerializerMethodField('get_user_id')
    user_id = serializers.SerializerMethodField()
    profile_image = serializers.SerializerMethodField('get_profile_image_url')
    class Meta:
        model = GraffitiUser
        fields = (
            'user_id',
            'email',
            'username',
            'created_at_utc',
            'user_type',
            'profile_image',
        )

    def get_user_id(self, object):
        return object.id

    def get_profile_image_url(self, object):
        path = str(object.profile_image)
        if path is '':
            return None
        path = path.split('/')
        image_name = path[-1]
        return settings.MEDIA_URL + 'image/' + image_name
