from enum import Enum

class UserType(Enum):
    GENERAL_USER = 'GU'
    COMMERCIAL_USER = 'CU'
    SUPER_USER = 'SU'

class UserSearchIdentifier(Enum):
    USER_ID = 'id'
    USER_EMAIL = 'email'
