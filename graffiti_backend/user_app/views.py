from copy import deepcopy
#from django.shortcuts import render
from django.contrib import auth
#from django.contrib.sessions.models import Session
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
#from rest_framework.decorators import api_view, parser_classes, permission_classes
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from graffiti_backend.commons import HTTPMethod, GraffitiMethod
from user_app.commons import UserType, UserSearchIdentifier
from user_app.logics.graffiti_user_factory import GraffitiUserFactory


@api_view(['POST', ])
#@permission_classes((IsAuthenticated, ))
@permission_classes((AllowAny, ))
#@permission_classes((AllowAny, IsAuthenticated,))
@csrf_exempt
def graffiti_user_view_handler(request):
    if request.method == HTTPMethod.POST:
        data_dictionary = deepcopy(request.data)
        if 'method' not in request.data:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if request.data['method'] == GraffitiMethod.CREATE:
            GraffitiUserFactory.create_graffiti_user(data_dictionary, request.data['user_type'])
            return Response(status=status.HTTP_201_CREATED)
        elif request.data['method'] == GraffitiMethod.UPDATE:
            auth_user = request.user
            auth_user_id = auth_user.id
            if auth_user_id is None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            logic_user = GraffitiUserFactory.get_logic_graffiti_user(UserSearchIdentifier.USER_ID, auth_user_id)
            GraffitiUserFactory.update_graffiti_user(data_dictionary, logic_user)
            GraffitiUserFactory.write_database_handler(UserSearchIdentifier.USER_ID, logic_user)
            return Response(status=status.HTTP_200_OK)
        elif request.data['method'] == GraffitiMethod.READ:
            auth_user = request.user
            auth_user_id = auth_user.id
            if auth_user_id is None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            logic_user = GraffitiUserFactory.get_logic_graffiti_user(UserSearchIdentifier.USER_ID, auth_user_id)
            user_data_json = GraffitiUserFactory.get_graffiti_user_json(logic_user)
            return Response(user_data_json, status=status.HTTP_200_OK)
        #TODO: delete user
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST', ])
@permission_classes((AllowAny, ))
@csrf_exempt
def login_view_handler(request):
    if request.method == HTTPMethod.POST:
        if 'email' not in request.data or 'password' not in request.data:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        email = request.data['email']
        password = request.data['password']
        user = auth.authenticate(email=email, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            logic_user = GraffitiUserFactory.get_logic_graffiti_user(UserSearchIdentifier.USER_EMAIL, user)
            user_data_json = GraffitiUserFactory.get_graffiti_user_json(logic_user)
            return Response(user_data_json, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST', ])
#@permission_classes((AllowAny, ))
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def logout_view_handler(request):
    if request.method == HTTPMethod.POST:
        try:
            auth.logout(request)
        except KeyError:
            pass
        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)
