from django.contrib.auth.models import AbstractBaseUser
from django.conf import settings
from django.db import models
from user_app.models.graffiti_user_manager import GraffitiUserManager

class GraffitiUser(AbstractBaseUser):
    GENERAL_USER = 'GU'
    COMMERCIAL_USER = 'CU'
    SUPER_USER = 'SU'
    USER_TYPE_CHOICES = (
        (GENERAL_USER, 'general_user'),
        (COMMERCIAL_USER, 'commercial_user'),
        (SUPER_USER, 'super_user')
    )

    email = models.EmailField(unique=True, verbose_name='email address', max_length=225, blank=False)
    username = models.CharField(max_length=30, null=True)
    created_at_utc = models.DateTimeField(auto_now_add=True)
    user_type = models.CharField(max_length=2, choices=USER_TYPE_CHOICES, default=GENERAL_USER)
    #contactInfo = models.OneToOneField(ContactInfo, null = True)
    profile_image = models.ImageField(upload_to=settings.MEDIA_ROOT+'/image', null=True)
    friends = models.ManyToManyField("self")
    objects = GraffitiUserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELD = ['email', 'username', 'password', 'user_type']

    class Meta:
        app_label = 'user_app'
