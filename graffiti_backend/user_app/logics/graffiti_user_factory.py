import os
from rest_framework.exceptions import ValidationError
from user_app.logics.graffiti_general_user import GraffitiGeneralUser
from user_app.models import GraffitiUser
from user_app.commons import UserType, UserSearchIdentifier
from user_app.serializers.graffiti_user_serializers import GraffitiUserCreateSerializer, GraffitiUserDetailSerializer

class GraffitiUserFactory(object):
    @staticmethod
    def create_graffiti_user(data_dictionary, user_type):
        if user_type == UserType.GENERAL_USER:
            logic_graffiti_user_instance = GraffitiUserFactory.__create_database_graffiti_general_user(data_dictionary)
        ################# TODO: commercial user ###################
        ################# TODO: super user ########################
        return logic_graffiti_user_instance

    @staticmethod
    def get_logic_graffiti_user(search_identifier, value):
        if search_identifier == UserSearchIdentifier.USER_ID:
            database_graffiti_user_instance = GraffitiUserFactory.read_database_handler(UserSearchIdentifier.USER_ID, value)
        elif search_identifier == UserSearchIdentifier.USER_EMAIL:
            database_graffiti_user_instance = GraffitiUserFactory.read_database_handler(UserSearchIdentifier.USER_EMAIL, value)
        else:
            return None
        if database_graffiti_user_instance.user_type == UserType.GENERAL_USER:
            logic_graffiti_user_instance = GraffitiUserFactory.__logic_graffiti_general_user_instance(database_graffiti_user_instance)
        ################# TODO: commercial user ###################
        ################# TODO: super user ########################
        return logic_graffiti_user_instance

    @staticmethod
    def update_graffiti_user(data_dictionary, logic_graffiti_user_instance):
        if 'email' in data_dictionary:
            logic_graffiti_user_instance.set_email(data_dictionary['email'])
        if 'username' in data_dictionary:
            logic_graffiti_user_instance.set_username(data_dictionary['username'])
        if 'password' in data_dictionary:
            logic_graffiti_user_instance.set_password(data_dictionary['password'])
        if 'user_type' in data_dictionary:
            logic_graffiti_user_instance.set_user_type(data_dictionary['user_type'])
        if 'profile_image' in data_dictionary:
            GraffitiUserFactory.write_image_handler(UserSearchIdentifier.USER_EMAIL, logic_graffiti_user_instance, data_dictionary)

    @staticmethod
    def get_graffiti_user_json(logic_graffiti_user_instance):
        if logic_graffiti_user_instance.get_user_type() == UserType.GENERAL_USER:
            serializer = GraffitiUserDetailSerializer(logic_graffiti_user_instance.get_database_graffiti_user_instance())
            return serializer.data

    @staticmethod
    def __create_database_graffiti_general_user(data_dictionary):
        data_dictionary['user_type'] = UserType.GENERAL_USER
        serializer = GraffitiUserCreateSerializer(data=data_dictionary)
        try:
            serializer.is_valid()
            database_graffiti_user_instance = serializer.save()
        except Exception as detail:
            error_message = "%s" % detail
            raise ValidationError(error_message)
        return GraffitiGeneralUser(database_graffiti_user_instance)

    @staticmethod
    def __logic_graffiti_general_user_instance(database_graffiti_user_instance):
        return GraffitiGeneralUser(database_graffiti_user_instance);

    @staticmethod
    def read_database_handler(search_identifier, value):
        if search_identifier == UserSearchIdentifier.USER_ID:
            return GraffitiUser.objects.get(id=value)
        if search_identifier == UserSearchIdentifier.USER_EMAIL:
            return GraffitiUser.objects.get(email=value)

    #@staticmethod
    #def read_database_handler_by_id(user_id):
    #    return GraffitiUser.objects.get(id=user_id)

    #@staticmethod
    #def read_database_handler_by_email(email):
    #    return GraffitiUser.objects.get(email=email)

    @staticmethod
    def write_database_handler(search_identifier, logic_graffiti_user_instance):
        if search_identifier == UserSearchIdentifier.USER_ID:
            database_graffiti_user_instance = GraffitiUserFactory.read_database_handler(UserSearchIdentifier.USER_ID, logic_graffiti_user_instance.user_id())
        elif search_identifier == UserSearchIdentifier.USER_EMAIL:
            database_graffiti_user_instance = GraffitiUserFactory.read_database_handler(UserSearchIdentifier.USER_EMAIL, logic_graffiti_user_instance.get_email())
        if logic_graffiti_user_instance.get_email() is not None:
            database_graffiti_user_instance.email = logic_graffiti_user_instance.get_email()
        if logic_graffiti_user_instance.get_username() is not None:
            database_graffiti_user_instance.username = logic_graffiti_user_instance.get_username()
        if logic_graffiti_user_instance.get_password() is not None:
            database_graffiti_user_instance.set_password(logic_graffiti_user_instance.get_password())
        if logic_graffiti_user_instance.get_user_type() is not None:
            database_graffiti_user_instance.user_type = logic_graffiti_user_instance.get_user_type()
        database_graffiti_user_instance.save()

    @staticmethod
    def write_image_handler(search_identifier, logic_graffiti_user_instance, data_dictionary):
        if search_identifier == UserSearchIdentifier.USER_ID:
            database_graffiti_user_instance = GraffitiUserFactory.read_database_handler(UserSearchIdentifier.USER_ID, logic_graffiti_user_instance.user_id())
        elif search_identifier == UserSearchIdentifier.USER_EMAIL:
            database_graffiti_user_instance = GraffitiUserFactory.read_database_handler(UserSearchIdentifier.USER_EMAIL, logic_graffiti_user_instance.get_email())
        if database_graffiti_user_instance.profile_image != '':
            os.remove(str(database_graffiti_user_instance.profile_image))
        if data_dictionary['profile_image'] == 'null':
            database_graffiti_user_instance.profile_image = None
        else:
            database_graffiti_user_instance.profile_image = data_dictionary['profile_image']
        database_graffiti_user_instance.save()
        logic_graffiti_user_instance.set_profile_image(database_graffiti_user_instance.profile_image)
