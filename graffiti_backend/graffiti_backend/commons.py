from enum import Enum

class HTTPMethod(Enum):
    POST = 'POST'
    PUT = 'PUT'
    GET = 'GET'
    DELETE = 'DELETE'

class GraffitiMethod(Enum):
    CREATE = 'create'
    UPDATE = 'update'
    READ = 'read'
    DELETE = 'delete'

class GraffitiOption(Enum):
    OVERVIEW = 'overview'
    DETAIL = 'detail'
    MODIFY = 'modify'
    LIKE = 'like'
    DISLIKE = 'dislike'
    ADD = 'add'
    CONFIRM = 'confirm'
    IGNORE = 'ignore'
    UNFRIEND = 'unfriend'
