"""graffiti_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
import pdb
from django.conf.urls import include, url, patterns
from django.conf import settings
#from django.views.decorators.csrf import csrf_exempt
#from django.contrib import admin
from rest_framework import routers, serializers, viewsets
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = patterns('',
    #url(r'^admin/', include(admin.site.urls)),
    url(r'^v2/users/$', 'user_app.views.graffiti_user_view_handler'),
    url(r'^v2/login/$', 'user_app.views.login_view_handler'),
    url(r'^v2/logout/$', 'user_app.views.logout_view_handler'),
    url(r'^v2/static/image/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT+'/image'}),

    url(r'^v2/users/(?P<user_id>\d+)/graffiti_messages$', 'message_app.views.user_graffiti_message_view_handler'),
    url(r'^v2/graffiti_messages/location$', 'message_app.views.nearby_graffiti_messages_view_handler'),
    url(r'^v2/graffiti_messages/(?P<message_id>\d+)$', 'message_app.views.user_graffiti_message_view_handler'),
    url(r'^v2/graffiti_messages/(?P<message_id>\d+)/(?P<option>(overview|detail))$', 'message_app.views.user_graffiti_message_view_handler'),

    url(r'^v2/users/(?P<user_id>\d+)/messages$', 'message_app.views.user_message_view_handler'),
    url(r'^v2/messages/location$', 'message_app.views.nearby_messages_view_handler'),
    url(r'^v2/messages/(?P<message_id>\d+)$', 'message_app.views.user_graffiti_message_view_handler'),
    url(r'^v2/messages/(?P<message_id>\d+)/(?P<option>(overview|detail))$', 'message_app.views.user_message_view_handler'),
)
