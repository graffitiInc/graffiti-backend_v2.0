from django.conf import settings
from django.contrib.gis import geos
from rest_framework import serializers
from rest_framework_gis.serializers import GeoModelSerializer
from message_app.models.message import Message

class MessageCreateSerializer(GeoModelSerializer):
    user_id = serializers.IntegerField()
    location = serializers.CharField()
    class Meta:
        model = Message
        fields = (
            'user_id',
            'author',
            'anonymous',
            'location',
            'title',
            'message_type',
            'text',
            'audio',
            'image',
            'video',
            'created_at_utc',
            'modified_at_utc',
            'until_time',
            'commented_message',
        )
        geo_field = 'location'


class MessageOverviewSerializer(GeoModelSerializer):
    #message_id = serializers.SerializerMethodField('get_message_id')
    message_id = serializers.SerializerMethodField()
    class Meta:
        model = Message
        fields = (
            'message_id',
            'location',
            'title',
            'message_type',
            'created_at_utc',
            'total_read_times',
            'total_liked_times',
            'total_disliked_times',
        )
        geo_field = 'location'

    def get_message_id(self, object):
        return object.id


class MessageDetailSerializer(GeoModelSerializer):
    #message_id = serializers.SerializerMethodField('get_message_id')
    message_id = serializers.SerializerMethodField()
    audio = serializers.SerializerMethodField('get_message_audio_url')
    image = serializers.SerializerMethodField('get_message_image_url')
    video = serializers.SerializerMethodField('get_message_video_url')
    class Meta:
        model = Message
        fields = (
            'message_id',
            'author',
            'anonymous',
            'location',
            'title',
            'message_type',
            'text',
            'audio',
            'image',
            'video',
            'created_at_utc',
            'total_read_times',
            'total_liked_times',
            'total_disliked_times',
            'modified_at_utc',
            'until_time',
            'commented_message',
        )
        geo_field = 'location'

    def get_message_id(self, object):
        return object.id

    def get_message_audio_url(self, object):
        path = str(object.audio)
        if path is '':
            return None
        path = path.split('/')
        audio_name = path[-1]
        return settings.MEDIA_URL + 'audio/' + audio_name

    def get_message_image_url(self, object):
        path = str(object.image)
        if path is '':
            return None
        path = path.split('/')
        image_name = path[-1]
        return settings.MEDIA_URL + 'image/' + image_name

    def get_message_video_url(self, object):
        path = str(object.video)
        if path is '':
            return None
        path = path.split('/')
        video_name = path[-1]
        return settings.MEDIA_URL + 'video/' + video_name

