import json
from django.conf import settings
from django.contrib.gis import geos
from rest_framework import serializers
from rest_framework_gis.serializers import GeoModelSerializer
from message_app.models.message import Message
from message_app.commons import MessageType
from user_app.models import GraffitiUser

class GraffitiMessageCreateSerializer(GeoModelSerializer):
    user_id = serializers.IntegerField()
    location = serializers.CharField()
    class Meta:
        model = Message
        fields = (
            'user_id',
            'anonymous',
            'location',
            'title',
            'text',
            'image',
            'created_at_utc',
            'modified_at_utc',
        )
        geo_field = 'location'

    def create(self, validated_data):
        user_id = validated_data.pop('user_id')
        user = GraffitiUser.objects.get(id=user_id)
        location_data = json.loads(validated_data.pop('location'))
        point = "POINT(%s %s)" % (float(location_data[0]), float(location_data[1]))
        location = geos.fromstr(point)
        message = Message(author=user, location=location, longitude=location_data[0], latitude=location_data[1], message_type=MessageType.GRAFFITI_MESSAGE, **validated_data)
        message.save()
        return message


class GraffitiMessageOverviewSerializer(GeoModelSerializer):
    #message_id = serializers.SerializerMethodField('get_message_id')
    message_id = serializers.SerializerMethodField()
    class Meta:
        model = Message
        fields = (
            'message_id',
            'location',
            'title',
            'message_type',
            'created_at_utc',
            'total_read_times',
            'total_liked_times',
            'total_disliked_times',
        )
        geo_field = 'location'

    def get_message_id(self, object):
        return object.id


class GraffitiMessageDetailSerializer(GeoModelSerializer):
    #message_id = serializers.SerializerMethodField('get_message_id')
    message_id = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField('get_message_image_url')
    class Meta:
        model = Message
        fields = (
            'message_id',
            'author',
            'anonymous',
            'location',
            'title',
            'message_type',
            'text',
            'image',
            'created_at_utc',
            'total_read_times',
            'total_liked_times',
            'total_disliked_times',
            'modified_at_utc',
        )
        geo_field = 'location'

    def get_message_id(self, object):
        return object.id

    def get_message_image_url(self, object):
        path = str(object.image)
        if path is '':
            return None
        path = path.split('/')
        image_name = path[-1]
        return settings.MEDIA_URL + 'image/' + image_name
