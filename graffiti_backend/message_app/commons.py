from enum import Enum

class MessageType(Enum):
    GRAFFITI_MESSAGE = 'GM'
    VOICE_MESSAGE = 'VO'
    MUSIC_MESSAGE = 'MM'
    VIDEO_MESSAGE = 'VM'
    COMMENT_MESSAGE = 'CO'
    COUPON_MESSAGE = 'CU'
    EVENT_MESSAGE = 'EV'
    ENCOUNTER_MESSAGE = 'EN'
    ADVERTISEMENT_MESSAGE = 'AD'

class MessageSearchIdentifier(Enum):
    AUTHOR_ID = 'author_id'
    LOCATION = 'location'
