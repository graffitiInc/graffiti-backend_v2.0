from django.conf import settings
from django.contrib.gis import geos
from django.contrib.gis.db import models as gis_models
from django.db import models
from datetime import datetime
#from message_app.commons import MessageType
from user_app.models import GraffitiUser
# When User app finish need import for authentication.

class Message(models.Model):
    GRAFFITI_MESSAGE = 'GM'
    VOICE_MESSAGE = 'VO'
    MUSIC_MESSAGE = 'MM'
    VIDEO_MESSAGE = 'VM'
    COMMENT_MESSAGE = 'CO'
    COUPON_MESSAGE = 'CU'
    EVENT_MESSAGE = 'EV'
    ENCOUNTER_MESSAGE = 'EN'
    ADVERTISEMENT_MESSAGE = 'AD'

    MESSAGE_TYPE_CHOICES = (
        (GRAFFITI_MESSAGE, 'graffiti_message'),
        (VOICE_MESSAGE, 'voice'),
        (MUSIC_MESSAGE, 'music'),
        (VIDEO_MESSAGE, 'video'),
        (COMMENT_MESSAGE, 'comment'),
        (COUPON_MESSAGE, 'coupon'),
        (EVENT_MESSAGE, 'event'),
        (ENCOUNTER_MESSAGE, 'encounter'),
        (ADVERTISEMENT_MESSAGE, 'advertisement'),
    )

    title = models.CharField(max_length=30, null=True)
    anonymous = models.BooleanField(default=False)
    author = models.ForeignKey(GraffitiUser)
    message_type = models.CharField(max_length=2, choices=MESSAGE_TYPE_CHOICES, default=GRAFFITI_MESSAGE)
    created_at_utc = models.DateTimeField(default=datetime.now, blank=True)
    modified_at_utc = models.DateTimeField(default=datetime.now, blank=True)
    until_time = models.DateTimeField(null=True, blank=True)     # For Coupon, Event and Encounter
    location = gis_models.PointField(u"longitude/latitude", geography=True, blank=True, null=True, srid=4326)
    longitude = models.FloatField(default=0)
    latitude = models.FloatField(default=0)
    total_read_times = models.IntegerField(default=0)
    total_liked_times = models.IntegerField(default=0)
    total_disliked_times = models.IntegerField(default=0)
    audio = models.FileField(upload_to=settings.MEDIA_ROOT+'/audio', null=True)
    image = models.ImageField(upload_to=settings.MEDIA_ROOT+'/image', null=True)
    video = models.FileField(upload_to=settings.MEDIA_ROOT+'/video', null=True)
    text = models.CharField(max_length=1000, null=True)
    commented_message = models.ForeignKey("self", null=True)

    gis = gis_models.GeoManager()
    objects = models.Manager()

