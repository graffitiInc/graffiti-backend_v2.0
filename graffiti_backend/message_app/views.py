#import os
#import json
from copy import deepcopy
#from django.shortcuts import render
#from django.contrib.gis import geos
from django.contrib.gis.measure import Distance, D
#from django.contrib.sessions.models import Session
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from graffiti_backend.commons import HTTPMethod, GraffitiMethod, GraffitiOption
from message_app.commons import MessageType, MessageSearchIdentifier
from message_app.logics.message_factory import MessageFactory


@api_view(['POST', 'GET', ])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def user_graffiti_message_view_handler(request, user_id=None, message_id=None, option=None):
    auth_user = request.user
    auth_user_id = auth_user.id
    data_dictionary = deepcopy(request.data)
    if request.method == HTTPMethod.POST:
        if user_id != None and message_id == None:
            if int(user_id) != auth_user_id or auth_user_id == None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            data_dictionary['user_id'] = auth_user_id
            if 'longitude' not in request.data or 'latitude' not in request.data:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            data_dictionary['location'] = '['+request.data['longitude']+', '+request.data['latitude']+']'
            if 'method' not in request.data:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            if request.data['method'] == GraffitiMethod.CREATE:
                MessageFactory.create_message(data_dictionary, MessageType.GRAFFITI_MESSAGE)
                return Response(status=status.HTTP_201_CREATED)
            elif request.data['method'] == GraffitiMethod.UPDATE:
                if 'option' not in request.data:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
                if request.data['option'] == GraffitiOption.MODIFY:
                    logic_message = MessageFactory.get_logic_message(request.data['message_id'])
                    MessageFactory.update_message(data_dictionary, logic_message)
                    MessageFactory.write_database_handler(logic_message)
                    return Response(status=status.HTTP_200_OK)
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        elif user_id == None and message_id != None:
            if auth_user_id == None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            if 'method' not in request.data and request.data['method'] != GraffitiMethod.UPDATE:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            logic_message = MessageFactory.get_logic_message(message_id)
            #TODO: like or dislike message.
            if request.data['option'] == GraffitiOption.LIKE:
                MessageFactory.like_message(logic_message)
                MessageFactory.write_database_handler(logic_message)
                return Response(status=status.HTTP_200_OK)
            elif request.data['option'] == GraffitiOption.DISLIKE:
                MessageFactory.dislike_message(logic_message)
                MessageFactory.write_database_handler(logic_message)
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        #TODO: delete message both need user_id and message_id, and message author is this user.
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    elif request.method == HTTPMethod.GET:
        if user_id != None and message_id == None:
            if int(user_id) != auth_user_id or auth_user_id == None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            data_dictionary['user_id'] = auth_user_id
            if 'offset' in request.GET and 'limit' in request.GET:
                data_dictionary['offset'] = request.GET['offset']
                data_dictionary['limit'] = request.GET['limit']
            elif 'offset' not in request.GET and 'limit' not in request.GET:
                data_dictionary['offset'] = None
                data_dictionary['limit'] = None
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            logic_messages = MessageFactory.get_logic_messages(MessageSearchIdentifier.AUTHOR_ID, data_dictionary, MessageType.GRAFFITI_MESSAGE)
            if request.GET['option'] == GraffitiOption.OVERVIEW:
                messages_overview_json = MessageFactory.get_messages_overview_json(logic_messages, MessageType.GRAFFITI_MESSAGE)
                return Response(messages_overview_json, status=status.HTTP_200_OK)
            elif request.GET['option'] == GraffitiOption.DETAIL:
                for logic_message in logic_messages:
                    logic_message.increase_total_read_times()
                    MessageFactory.write_database_handler(logic_message)
                messages_detail_json = MessageFactory.get_messages_detail_json(logic_messages, MessageType.GRAFFITI_MESSAGE)
                return Response(messages_detail_json, status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        elif user_id == None and message_id != None:
            if auth_user_id == None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            if option == None:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            logic_message = MessageFactory.get_logic_message(message_id)
            if option == GraffitiOption.OVERVIEW:
                graffiti_message_overview_json = MessageFactory.get_message_overview_json(logic_message)
                return Response(graffiti_message_overview_json, status=status.HTTP_200_OK)
            elif option == GraffitiOption.DETAIL:
                logic_message.increase_total_read_times()
                MessageFactory.write_database_handler(logic_message)
                graffiti_message_detail_json = MessageFactory.get_message_detail_json(logic_message)
                return Response(graffiti_message_detail_json, status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', ])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def nearby_graffiti_messages_view_handler(request):
    if request.method == HTTPMethod.GET:
        auth_user = request.user
        auth_user_id = auth_user.id
        data_dictionary = deepcopy(request.data)
        data_dictionary['user_id'] = auth_user_id
        if auth_user_id is None:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        if 'longitude' not in request.GET or 'latitude' not in request.GET or 'radius' not in request.GET:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        data_dictionary['location'] = '['+request.GET['longitude']+', '+request.GET['latitude']+']'
        data_dictionary['radius'] = request.GET['radius']
        if 'offset' in request.GET and 'limit' in request.GET:
            data_dictionary['offset'] = request.GET['offset']
            data_dictionary['limit'] = request.GET['limit']
        elif 'offset' not in request.GET and 'limit' not in request.GET:
            data_dictionary['offset'] = None
            data_dictionary['limit'] = None
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        logic_messages = MessageFactory.get_logic_messages(MessageSearchIdentifier.LOCATION, data_dictionary, MessageType.GRAFFITI_MESSAGE)
        if 'option' not in request.GET:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if request.GET['option'] == GraffitiOption.OVERVIEW:
            graffiti_messages_overview_json = MessageFactory.get_messages_overview_json(logic_messages, MessageType.GRAFFITI_MESSAGE)
            return Response(graffiti_messages_overview_json, status=status.HTTP_200_OK)
        elif request.GET['option'] == GraffitiOption.DETAIL:
            for logic_message in logic_messages:
                logic_message.increase_total_read_times()
                MessageFactory.write_database_handler(logic_message)
            graffiti_messages_detail_json = MessageFactory.get_messages_detail_json(logic_messages, MessageType.GRAFFITI_MESSAGE)
            return Response(graffiti_messages_detail_json, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST', 'GET'])
@permission_classes((IsAuthenticated, ))
#@permission_classes((AllowAny, ))
@csrf_exempt
def user_message_view_handler(request, user_id=None, message_id=None, option=None):
    auth_user = request.user
    auth_user_id = auth_user.id
    data_dictionary = deepcopy(request.data)
    if request.method == HTTPMethod.POST:
        if user_id != None and message_id == None:
            if int(user_id) != auth_user_id or auth_user_id == None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            data_dictionary['user_id'] = auth_user_id
            if 'longitude' not in request.data or 'latitude' not in request.data:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            data_dictionary['location'] = '['+request.data['longitude']+', '+request.data['latitude']+']'
            if 'method' not in request.data:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            if request.data['method'] == GraffitiMethod.CREATE:
                if 'message_type' not in request.data:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
                MessageFactory.create_message(data_dictionary, request.data['message_type'])
                return Response(status=status.HTTP_201_CREATED)
            elif request.data['method'] == GraffitiMethod.UPDATE:
                if 'option' not in request.data:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
                if request.data['option'] == GraffitiOption.MODIFY:
                    logic_message = MessageFactory.get_logic_message(request.data['message_id'])
                    MessageFactory.update_message(data_dictionary, logic_message)
                    MessageFactory.write_database_handler(logic_message)
                    return Response(status=status.HTTP_200_OK)
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        elif user_id == None and message_id != None:
            if auth_user_id == None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            if 'method' not in request.data and request.data['method'] != GraffitiMethod.UPDATE:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            logic_message = MessageFactory.get_logic_message(message_id)
            #TODO: like or dislike message, but only for specific type.
            if request.data['option'] == GraffitiOption.LIKE:
                MessageFactory.like_message(logic_message)
                MessageFactory.write_database_handler(logic_message)
                return Response(status=status.HTTP_200_OK)
            elif request.data['option'] == GraffitiOption.DISLIKE:
                MessageFactory.dislike_message(logic_message)
                MessageFactory.write_database_handler(logic_message)
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        #TODO: delete message both need user_id and message_id, and message author is this user.
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    elif request.method == HTTPMethod.GET:
        if user_id != None and message_id == None:
            if int(user_id) != auth_user_id or auth_user_id == None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            data_dictionary['user_id'] = auth_user_id
            if 'offset' in request.GET and 'limit' in request.GET:
                data_dictionary['offset'] = request.GET['offset']
                data_dictionary['limit'] = request.GET['limit']
            elif 'offset' not in request.GET and 'limit' not in request.GET:
                data_dictionary['offset'] = None
                data_dictionary['limit'] = None
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            if 'message_type' not in request.GET:
                logic_messages = MessageFactory.get_logic_messages(MessageSearchIdentifier.AUTHOR_ID, data_dictionary)
                if request.GET['option'] == GraffitiOption.OVERVIEW:
                    messages_overview_json = MessageFactory.get_messages_overview_json(logic_messages)
                    return Response(messages_overview_json, status=status.HTTP_200_OK)
                elif request.GET['option'] == GraffitiOption.DETAIL:
                    for logic_message in logic_messages:
                        logic_message.increase_total_read_times()
                        MessageFactory.write_database_handler(logic_message)
                    messages_detail_json = MessageFactory.get_messages_detail_json(logic_messages)
                    return Response(messages_detail_json, status=status.HTTP_200_OK)
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                logic_messages = MessageFactory.get_logic_messages(MessageSearchIdentifier.AUTHOR_ID, data_dictionary, request.GET['message_type'])
                if request.GET['option'] == GraffitiOption.OVERVIEW:
                    messages_overview_json = MessageFactory.get_messages_overview_json(logic_messages, request.GET['message_type'])
                    return Response(messages_overview_json, status=status.HTTP_200_OK)
                elif request.GET['option'] == GraffitiOption.DETAIL:
                    for logic_message in logic_messages:
                        logic_message.increase_total_read_times()
                        MessageFactory.write_database_handler(logic_message)
                    messages_detail_json = MessageFactory.get_messages_detail_json(logic_messages, request.GET['message_type'])
                    return Response(messages_detail_json, status=status.HTTP_200_OK)
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
        elif user_id == None and message_id != None:
            if auth_user_id == None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            if option == None:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            logic_message = MessageFactory.get_logic_message(message_id)
            if option == GraffitiMethod.OVERVIEW:
                message_overview_json = MessageFactory.get_message_overview_json(logic_message)
                return Response(message_overview_json, status=status.HTTP_200_OK)
            elif option == GraffitiMethod.DETAIL:
                logic_message.increase_total_read_times()
                MessageFactory.write_database_handler(logic_message)
                message_detail_json = MessageFactory.get_message_detail_json(logic_message)
                return Response(message_detail_json, status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', ])
@permission_classes((IsAuthenticated, ))
@csrf_exempt
def nearby_messages_view_handler(request):
    if request.method == HTTPMethod.GET:
        auth_user = request.user
        auth_user_id = auth_user.id
        data_dictionary = deepcopy(request.data)
        data_dictionary['user_id'] = auth_user_id
        if auth_user_id is None:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        if 'longitude' not in request.GET or 'latitude' not in request.GET or 'radius' not in request.GET:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        data_dictionary['location'] = '['+request.GET['longitude']+', '+request.GET['latitude']+']'
        data_dictionary['radius'] = request.GET['radius']
        if 'offset' in request.GET and 'limit' in request.GET:
            data_dictionary['offset'] = request.GET['offset']
            data_dictionary['limit'] = request.GET['limit']
        elif 'offset' not in request.GET and 'limit' not in request.GET:
            data_dictionary['offset'] = None
            data_dictionary['limit'] = None
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if 'message_type' not in request.GET:
            logic_messages = MessageFactory.get_logic_messages(MessageSearchIdentifier.AUTHOR_ID, data_dictionary)
            if 'option' not in request.GET:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            if request.GET['option'] == GraffitiOption.OVERVIEW:
                messages_overview_json = MessageFactory.get_messages_overview_json(logic_messages)
                return Response(messages_overview_json, status=status.HTTP_200_OK)
            elif request.GET['option'] == GraffitiOption.DETAIL:
                for logic_message in logic_messages:
                    #TODO: also need check other type need increase read times.
                    if logic_message.get_message_type() == MessageType.GRAFFITI_MESSAGE:
                        logic_message.increase_total_read_times()
                    MessageFactory.write_database_handler(logic_message)
                messages_detail_json = MessageFactory.get_messages_detail_json(logic_messages)
                return Response(messages_detail_json, status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            logic_messages = MessageFactory.get_logic_messages(MessageSearchIdentifier.LOCATION, data_dictionary, request.GET['message_type'])
            #TODO: also need check other type need increase read times.
            if 'option' not in request.GET:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            if request.GET['option'] == GraffitiOption.OVERVIEW:
                messages_overview_json = MessageFactory.get_messages_overview_json(logic_messages, request.GET['message_type'])
                return Response(messages_overview_json, status=status.HTTP_200_OK)
            elif request.GET['option'] == GraffitiOption.DETAIL:
                for logic_message in logic_messages:
                    logic_message.increase_total_read_times()
                    MessageFactory.write_database_handler(logic_message)
                messages_detail_json = MessageFactory.get_messages_detail_json(logic_messages, request.GET['message_type'])
                return Response(messages_detail_json, status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_400_BAD_REQUEST)

