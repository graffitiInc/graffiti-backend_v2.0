import os
import json
from django.contrib.gis import geos
from django.contrib.gis.measure import Distance, D
from rest_framework.exceptions import ValidationError
from message_app.commons import MessageType, MessageSearchIdentifier
from message_app.logics.graffiti_message import GraffitiMessage
from message_app.models import Message
from message_app.serializers.all_type_message_serializers import MessageCreateSerializer, MessageOverviewSerializer, MessageDetailSerializer
from message_app.serializers.graffiti_message_serializers import GraffitiMessageCreateSerializer, GraffitiMessageOverviewSerializer, GraffitiMessageDetailSerializer



class MessageFactory(object):
    @staticmethod
    def create_message(data_dictionary, message_type):
        #user = data.user
        #user_id = user.id
        #data_dictionary['user_id'] = user_id
        #data_dictionary['location'] = '['+data_dictionary['longitude']+', '+data_dictionary['latitude']+']'
        if message_type == MessageType.GRAFFITI_MESSAGE:
            logic_message_instance = MessageFactory.__create_database_graffiti_message(data_dictionary)
        '''
        elif message_type == MessageType.VOICE_MESSAGE:
            logic_message_instance = MessageFactory.__create_database_voice_message(data_dictionary)
        elif message_type == MessageType.MUSIC_MESSAGE:
            logic_message_instance = MessageFactory.__create_database_music_message(data_dictionary)
        elif message_type == MessageType.VIDEO_MESSAGE:
            logic_message_instance = MessageFactory.__create_database_video_message(data_dictionary)
        elif message_type == MessageType.COMMENT_MESSAGE:
            logic_message_instance = MessageFactory.__create_database_comment_message(data_dictionary)
        elif message_type == MessageType.COUPON_MESSAGE:
            logic_message_instance = MessageFactory.__create_database_coupon_message(data_dictionary)
        elif message_type == MessageType.EVENT_MESSAGE:
            logic_message_instance = MessageFactory.__create_database_event_message(data_dictionary)
        elif message_type == MessageType.ENCOUNTER_MESSAGE:
            logic_message_instance = MessageFactory.__create_database_encounter_message(data_dictionary)
        elif message_type == MessageType.ADVERTISEMENT_MESSAGE:
            logic_message_instance = MessageFactory.__create_database_advertisement_message(data_dictionary)
        '''
        return logic_message_instance

    @staticmethod
    def get_logic_message(message_id):
        database_message_instance = MessageFactory.read_database_handler(message_id)
        if database_message_instance.message_type == MessageType.GRAFFITI_MESSAGE:
            logic_message_instance = MessageFactory.__logic_graffiti_message_instance(database_message_instance)
        '''
        elif database_message_instance.message_type == MessageType.VOICE_MESSAGE:
            logic_message_instance = MessageFactory.__logic_voice_message_instance(database_message_instance)
        elif database_message_instance.message_type == MessageType.MUSIC_MESSAGE:
            logic_message_instance = MessageFactory.__logic_music_message_instance(database_message_instance)
        elif database_message_instance.message_type == MessageType.VIDEO_MESSAGE:
            logic_message_instance = MessageFactory.__logic_video_message_instance(database_message_instance)
        elif database_message_instance.message_type == MessageType.COMMENT_MESSAGE:
            logic_message_instance = MessageFactory.__logic_comment_message_instance(database_message_instance)
        elif database_message_instance.message_type == MessageType.COUPON_MESSAGE:
            logic_message_instance = MessageFactory.__logic_coupon_message_instance(database_message_instance)
        elif database_message_instance.message_type == MessageType.EVENT_MESSAGE:
            logic_message_instance = MessageFactory.__logic_event_message_instance(database_message_instance)
        elif database_message_instance.message_type == MessageType.ENCOUNTER_MESSAGE:
            logic_message_instance = MessageFactory.__logic_encounter_message_instance(database_message_instance)
        elif database_message_instance.message_type == MessageType.ADVERTISEMENT_MESSAGE:
            logic_message_instance = MessageFactory.__logic_advertisement_message_instance(database_message_instance)
        '''
        return logic_message_instance

    @staticmethod
    def get_logic_messages(search_identifier, data_dictionary, message_type=None):
        logic_message_instances = list()
        if message_type != None:
            if search_identifier == MessageSearchIdentifier.AUTHOR_ID:
                author_id = data_dictionary['user_id']
                offset = data_dictionary['offset']
                limit = data_dictionary['limit']
                database_message_instances = MessageFactory.read_database_batch_handler_by_author_id(author_id, message_type, offset, limit)
            elif search_identifier == MessageSearchIdentifier.LOCATION:
                location = data_dictionary['location']
                radius = data_dictionary['radius']
                offset = data_dictionary['offset']
                limit = data_dictionary['limit']
                database_message_instances = MessageFactory.read_database_batch_handler_by_location(location, radius, message_type, offset, limit)
            else:
                return None
            for database_message_instance in database_message_instances:
                if database_message_instance.message_type == MessageType.GRAFFITI_MESSAGE:
                    logic_message_instance = MessageFactory.__logic_graffiti_message_instance(database_message_instance)
                #TODO: different type of messages
                logic_message_instances.append(logic_message_instance)
        else:
            if search_identifier == MessageSearchIdentifier.AUTHOR_ID:
                author_id = data_dictionary['user_id']
                offset = data_dictionary['offset']
                limit = data_dictionary['limit']
                database_message_instances = MessageFactory.read_database_batch_handler_by_author_id(author_id=author_id, offset=offset, limit=limit)
            elif search_identifier == MessageSearchIdentifier.LOCATION:
                location = data_dictionary['location']
                radius = data_dictionary['radius']
                offset = data_dictionary['offset']
                limit = data_dictionary['limit']
                database_message_instances = MessageFactory.read_database_batch_handler_by_location(location=location, radius=radius, offset=offset, limit=limit)
            else:
                return None
            for database_message_instance in database_message_instances:
                logic_message_instance = MessageFactory.__logic_graffiti_message_instance(database_message_instance)
                logic_message_instances.append(logic_message_instance)
        return logic_message_instances

    @staticmethod
    def update_message(data_dictionary, logic_message_instance):
        if 'title' in data_dictionary:
            logic_message_instance.set_title(data_dictionary['title'])
        if 'anonymous' in data_dictionary:
            if data_dictionary['anonymous'] is 'True':
                logic_message_instance.set_anonymous()
            else:
                logic_message_instance.set_onymous()
        if 'author' in data_dictionary:
            logic_message_instance.set_author(data_dictionary['author'])
        if 'message_type' in data_dictionary:
            logic_message_instance.set_message_type(data_dictionary['message_type'])
        if 'location' in data_dictionary:
            location_data = json.loads(data_dictionary['location'])
            logic_message_instance.set_location(location_data[0], location_data[1])
        if 'text' in data_dictionary:
            logic_message_instance.set_text(data_dictionary['text'])
        if 'commented_message' in data_dictionary:
            logic_message_instance.set_commented_message(data_dictionary['commented_message'])
        MessageFactory.write_file_handler(logic_message_instance, data_dictionary)

    @staticmethod
    def like_message(logic_message_instance):
        if logic_message_instance.get_message_type() == MessageType.GRAFFITI_MESSAGE:
            logic_message_instance.increase_total_liked_times()
        #TODO: other type of message need increase liked times

    @staticmethod
    def dislike_message(logic_message_instance):
        if logic_message_instance.get_message_type() == MessageType.GRAFFITI_MESSAGE:
            logic_message_instance.increase_total_disliked_times()
        #TODO: other type of message need increase liked times
    ################################# TODO: delete message. #############################################
    #@staticmethod
    #def delete_message(message_id):
    #####################################################################################################

    @staticmethod
    def get_message_overview_json(logic_message_instance):
        if logic_message_instance.get_message_type() == MessageType.GRAFFITI_MESSAGE:
            serializer = GraffitiMessageOverviewSerializer(logic_message_instance.get_database_message_instance())
        '''
        elif logic_message_instance.get_message_type() == MessageType.VOICE_MESSAGE:
            serializer = VoiceMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.MUSIC_MESSAGE:
            serializer = MusicMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.VIDEO_MESSAGE:
            serializer = VideoMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.COMMENT_MESSAGE:
            serializer = CommentMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.COUPON_MESSAGE:
            serializer = CouponMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.EVENT_MESSAGE:
            serializer = EventMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.ENCOUNTER_MESSAGE:
            serializer = EncounterMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.ADVERTISEMENT_MESSAGE:
            serializer = AdvertisementMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        '''
        return serializer.data

    @staticmethod
    def get_message_detail_json(logic_message_instance):
        if logic_message_instance.get_message_type() == MessageType.GRAFFITI_MESSAGE:
            serializer = GraffitiMessageDetailSerializer(logic_message_instance.get_database_message_instance())
        '''
        elif logic_message_instance.get_message_type() == MessageType.VOICE_MESSAGE:
            serializer = VoiceMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.MUSIC_MESSAGE:
            serializer = MusicMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.VIDEO_MESSAGE:
            serializer = VideoMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.COMMENT_MESSAGE:
            serializer = CommentMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.COUPON_MESSAGE:
            serializer = CouponMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.EVENT_MESSAGE:
            serializer = EventMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.ENCOUNTER_MESSAGE:
            serializer = EncounterMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        elif logic_message_instance.get_message_type() == MessageType.ADVERTISEMENT_MESSAGE:
            serializer = AdvertisementMessageQuerySerialiser(logic_message_instance.get_database_message_instance())
        '''
        return serializer.data

    @staticmethod
    def get_messages_overview_json(logic_message_instances, message_type=None):
        database_message_instances = list()
        if message_type != None:
            if message_type == MessageType.GRAFFITI_MESSAGE:
                for logic_message_instance in logic_message_instances:
                    database_message_instances.append(logic_message_instance.get_database_message_instance())
                serializer = GraffitiMessageOverviewSerializer(database_message_instances, many=True)
            #TODO: different type of message
        else:
            for logic_message_instance in logic_message_instances:
                database_message_instances.append(logic_message_instance.get_database_message_instance())
            serializer = MessageOverviewSerializer(database_message_instances, many=True)
        return serializer.data

    @staticmethod
    def get_messages_detail_json(logic_message_instances, message_type=None):
        database_message_instances = list()
        if message_type != None:
            if message_type == MessageType.GRAFFITI_MESSAGE:
                for logic_message_instance in logic_message_instances:
                    database_message_instances.append(logic_message_instance.get_database_message_instance())
                serializer = GraffitiMessageDetailSerializer(database_message_instances, many=True)
            #TODO: different type of message
        else:
            for logic_message_instance in logic_message_instances:
                database_message_instances.append(logic_message_instance.get_database_message_instance())
            serializer = MessageDetailSerializer(database_message_instances, many=True)
        return serializer.data

    @staticmethod
    def __create_database_graffiti_message(data_dictionary):
        #data_dictionary['message_type'] = MessageType.GRAFFITI_MESSAGE
        serializer = GraffitiMessageCreateSerializer(data=data_dictionary)
        try:
            serializer.is_valid()
            database_graffiti_message_instance = serializer.save()
        except Exception as detail:
            error_message = "%s" % detail
            raise ValidationError(error_message)
        return GraffitiMessage(database_graffiti_message_instance)

    @staticmethod
    def __logic_graffiti_message_instance(database_graffiti_message_instance):
        return GraffitiMessage(database_graffiti_message_instance)

    '''
    @staticmethod
    def __create_database_voice_message(data_dictionary):
        data_dictionary['message_type'] = MessageType.VOICE_MESSAGE
        serializer = VoiceMessageCreateSerializer(data=data_dictionary)
        try:
            serializer.is_valid()
            database_video_message_instance = serializer.save()
            #database_voice_message_instance.audio = data_dictionary['audio']
            #database_voice_message_instance.save()
        except Exception as detail:
            error_message = "%s" % detail
            raise ValidationError(error_message)
        return VoiceMessage(database_voice_message_instance)
    '''
    '''
    @staticmethod
    def __logic_voice_message_instance(database_voice_message_instance):
        return VoiceMessage(database_voice_message_instance)
    '''
    '''
    @staticmethod
    def __create_database_music_message(data_dictionary):
        data_dictionary['message_type'] = MessageType.MUSIC_MESSAGE
        serializer = MusicMessageCreateSerializer(data=data_dictionary)
        try:
            serializer.is_valid()
            database_music_message_instance = serializer.save()
            #database_music_message_instance.audio = data_dictionary['audio']
            #database_music_message_instance.save()
        except Exception as detail:
            error_message = "%s" % detail
            raise ValidationError(error_message)
        return MusicMessage(database_music_message_instance)
    '''
    '''
    @staticmethod
    def __logic_music_message_instance(database_music_message_instance):
        return MusicMessage(database_music_message_instance)
    '''
    '''
    @staticmethod
    def __create_database_video_message(data_dictionary):
        data_dictionary['message_type'] = MessageType.VIDEO_MESSAGE
        serializer = VideoMessageCreateSerializer(data=data_dictionary)
        try:
            serializer.is_valid()
            database_video_message_instance = serializer.save()
            #database_video_message_instance.video = data_dictionary['video']
            #database_video_message_instance.save()
        except Exception as detail:
            error_message = "%s" % detail
            raise ValidationError(error_message)
        return VideoMessage(database_video_message_instance)
    '''
    '''
    @staticmethod
    def __logic_video_message_instance(database_video_message_instance):
        return VideoMessage(database_video_message_instance)
    '''
    '''
    @staticmethod
    def __create_database_comment_message(data_dictionary):
        data_dictionary['message_type'] = MessageType.COMMENT_MESSAGE
        serializer = CommentMessageCreateSerializer(data=data_dictionary)
        try:
            serializer.is_valid()
            database_comment_message_instance = serializer.save()
            #if 'image' in data_dictionary:
            #    database_comment_message_instance.image = data_dictionary['image']
            #if 'audio' in data_dictionary:
            #    database_comment_message_instance.audio = data_dictionary['audio']
            #database_comment_message_instance.save()
        except Exception as detail:
            error_message = "%s" % detail
            raise ValidationError(error_message)
        return CommentMessage(database_comment_message_instance)
    '''
    '''
    @staticmethod
    def __logic_comment_message_instance(database_comment_message_instance):
        return CommentMessage(database_comment_message_instance)
    '''
    '''
    @staticmethod
    def __create_database_coupon_message(data_dictionary):
        data_dictionary['message_type'] = MessageType.COUPON_MESSAGE
        serializer = CouponMessageCreateSerializer(data=data_dictionary)
        try:
            serializer.is_valid()
            database_coupon_message_instance = serializer.save()
            #database_coupon_message_instance.image = data_dictionary['image']
            #database_coupon_message_instance.save()
        except Exception as detail:
            error_message = "%s" % detail
            raise ValidationError(error_message)
        return CouponMessage(database_coupon_message_instance)
    '''
    '''
    @staticmethod
    def __logic_coupon_message_instance(database_coupon_message_instance):
        return CouponMessage(database_coupon_message_instance)
    '''
    '''
    @staticmethod
    def __create_database_event_message(data_dictionary):
        data_dictionary['message_type'] = MessageType.EVENT_MESSAGE
        serializer = EventMessageCreateSerializer(data=data_dictionary)
        try:
            serializer.is_valid()
            database_event_message_instance = serializer.save()
            #if 'image' in data_dictionary:
            #    database_event_message_instance.image = data_dictionary['image']
            #if 'video' in data_dictionary:
            #    database_event_message_instance.video = data_dictionary['video']
            #database_event_message_instance.save()
        except Exception as detail:
            error_message = "%s" % detail
            raise ValidationError(error_message)
        return EventMessage(database_event_message_instance)
    '''
    '''
    @staticmethod
    def __logic_event_message_instance(database_event_message_instance):
        return EventMessage(database_event_message_instance)
    '''
    '''
    @staticmethod
    def __create_database_encounter_message(data_dictionary):
        data_dictionary['message_type'] = MessageType.ENCOUNTER_MESSAGE
        serializer = EncounterMessageCreateSerializer(data=data_dictionary)
        try:
            serializer.is_valid()
            database_encounter_message_instance = serializer.save()
            #database_encounter_message_instance.image = data_dictionary['image']
            #database_message_instance.save()
        except Exception as detail:
            error_message = "%s" % detail
            raise ValidationError(error_message)
        return EncounterMessage(database_encounter_message_instance)
    '''
    '''
    @staticmethod
    def __logic_encounter_message_instance(database_encounter_message_instance):
        return EncounterMessage(database_encounter_message_instance)
    '''
    '''
    @staticmethod
    def __create_database_advertisement_message(data_dictionary):
        data_dictionary['message_type'] = MessageType.ADVERTISEMENT_MESSAGE
        serializer = AdvertisementMessageCreateSerializer(data=data_dictionary)
        try:
            serializer.is_valid()
            database_advertisement_message_instance = serializer.save()
            #if 'image' in data_dictionary:
            #    database_advertisement_message_instance.image = data_dictionary['image']
            #if 'video' in data_dictionary:
            #    database_advertisement_message_instance.video = data_dictionary['video']
            #database_advertisement_message_instance.save()
        except Exception as detail:
            error_message = "%s" % detail
            raise ValidationError(error_message)
        return AdvertisementMessage(database_advertisement_message_instance)
    '''
    '''
    @staticmethod
    def __logic_advertisement_message_instance(database_advertisement_message_instance):
        return AdvertisementMessage(database_advertisement_message_instance)
    '''
    @staticmethod
    def read_database_handler(message_id):
        return Message.objects.get(id=message_id)

    @staticmethod
    def read_database_batch_handler_by_author_id(author_id, message_type=None, offset=None, limit=None):
        if message_type != None:
            if offset != None and limit != None:
                offset = int(offset)
                limit = int(limit) + int(offset)
                return Message.objects.filter(author_id=author_id, message_type=message_type).order_by('created_at_utc')[offset:limit]
            elif offset == None and limit != None:
                limit = int(limit)
                return Message.objects.filter(author_id=author_id, message_type=message_type).order_by('created_at_utc')[:limit]
            elif offset == None and limit == None:
                return Message.objects.filter(author_id=author_id, message_type=message_type).order_by('created_at_utc')
            else:
                return None
        else:
            if offset != None and limit != None:
                offset = int(offset)
                limit = int(limit) + int(offset)
                return Message.objects.filter(author_id=author_id).order_by('created_at_utc')[offset:limit]
            elif offset == None and limit != None:
                limit = int(limit)
                return Message.objects.filter(author_id=author_id).order_by('created_at_utc')[:limit]
            elif offset == None and limit == None:
                return Message.objects.filter(author_id=author_id).order_by('created_at_utc')
            else:
                return None

    @staticmethod
    def read_database_batch_handler_by_location(location, radius, message_type=None, offset=None, limit=None):
        location_data = json.loads(location)
        point = "POINT(%s %s)" % (float(location_data[0]), float(location_data[1]))
        location_geos = geos.fromstr(point, srid = 4326)
        if message_type != None:
            if offset != None and limit != None:
                offset = int(offset)
                limit = int(limit) + int(offset)
                return Message.objects.filter(location__distance_lte=(location_geos, Distance(m = radius)), message_type=message_type).order_by('-total_liked_times', '-total_read_times')[offset:limit]
            elif offset == None and limit != None:
                limit = int(limit)
                return Message.objects.filter(location__distance_lte=(location_geos, Distance(m = radius)), message_type=message_type).order_by('-total_liked_times', '-total_read_times')[:limit]
            elif offset == None and limit == None:
                return Message.objects.filter(location__distance_lte=(location_geos, Distance(m = radius)), message_type=message_type).order_by('-total_liked_times', '-total_read_times')
            else:
                return None
        else:
            if offset != None and limit != None:
                offset = int(offset)
                limit = int(limit) + int(offset)
                return Message.objects.filter(location__distance_lte=(location_geos, Distance(m = radius))).order_by('-total_liked_times', '-total_read_times')[offset:limit]
            elif offset == None and limit != None:
                limit = int(limit)
                return Message.objects.filter(location__distance_lte=(location_geos, Distance(m = radius))).order_by('-total_liked_times', '-total_read_times')[:limit]
            elif offset == None and limit == None:
                return Message.objects.filter(location__distance_lte=(location_geos, Distance(m = radius))).order_by('-total_liked_times', '-total_read_times')
            else:
                return None

    @staticmethod
    def write_database_handler(logic_message_instance):
        database_message_instance =MessageFactory.read_database_handler(logic_message_instance.message_id())
        if logic_message_instance.get_title() != None:
            database_message_instance.title = logic_message_instance.get_title()
        if logic_message_instance.get_anonymous_status() != None:
            database_message_instance.anonymous = logic_message_instance.get_anonymous_status()
        if logic_message_instance.get_author() != None:
            database_message_instance.author = logic_message_instance.get_author()
        if logic_message_instance.get_message_type() != None:
            database_message_instance.message_type = logic_message_instance.get_message_type()
        if logic_message_instance.get_until_time() != None:
            database_message_instance.until_time = logic_message_instance.get_until_time()
        if logic_message_instance.get_location() != None:
            database_message_instance.location = logic_message_instance.get_location()
        if logic_message_instance.get_longitude() != None:
            database_message_instance.longitude = logic_message_instance.get_longitude()
        if logic_message_instance.get_latitude() != None:
            database_message_instance.latitude = logic_message_instance.get_latitude()
        if logic_message_instance.get_total_read_times() != None:
            database_message_instance.total_read_times = logic_message_instance.get_total_read_times()
        if logic_message_instance.get_total_liked_times() != None:
            database_message_instance.total_liked_times = logic_message_instance.get_total_liked_times()
        if logic_message_instance.get_total_disliked_times() != None:
            database_message_instance.total_disliked_times = logic_message_instance.get_total_disliked_times()
        if logic_message_instance.get_text() != None:
            database_message_instance.text = logic_message_instance.get_text()
        if logic_message_instance.get_commented_message() != None:
            database_message_instance.commented_message = logic_message_instance.get_commented_message()
        database_message_instance.save()

    @staticmethod
    def write_file_handler(logic_message_instance, data_dictionary):
        database_message_instance = MessageFactory.read_database_handler(logic_message_instance.message_id())
        if 'audio' in data_dictionary:
            if database_message_instance.audio != '':
                os.remove(str(database_message_instance.audio))
            if data_dictionary['audio'] == 'null':
                database_message_instance.audio = None
            else:
                database_message_instance.audio = data_dictionary['audio']
            logic_message_instance.set_audio(data_dictionary['audio'])
        if 'image' in data_dictionary:
            if database_message_instance.image != '':
                os.remove(str(database_message_instance.image))
            if data_dictionary['image'] == 'null':
                database_message_instance.image = None
            else:
                database_message_instance.image = data_dictionary['image']
            logic_message_instance.set_image(data_dictionary['image'])
        if 'video' in data_dictionary:
            if database_message_instance.video != '':
                os.remove(str(database_message_instance.video))
            if data_dictionary['video'] == 'null':
                database_message_instance.video = None
            else:
                database_message_instance.video = data_dictionary['video']
            logic_message_instance.set_video(data_dictionary['video'])
        database_message_instance.save()

