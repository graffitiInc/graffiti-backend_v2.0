from django.contrib.gis import geos
from abc import abstractmethod


class AbstractMessage(object):
    """TODO: AbstractMessage
    AbstractMessage can create and update message to database.
    """
    def __init__(self, database_message_instance):
        self.__database_message_instance = database_message_instance
        self.__message_id = database_message_instance.id
        self.__anonymous = database_message_instance.anonymous
        self.__author = database_message_instance.author
        self.__message_type = database_message_instance.message_type
        self.__location = database_message_instance.location
        self.__longitude = database_message_instance.longitude
        self.__latitude = database_message_instance.latitude
        self.__created_at_utc = database_message_instance.created_at_utc
        self.__modified_at_utc = database_message_instance.modified_at_utc

    def set_database_message_instance(self, database_message_instance):
        self.__database_message_instance = database_message_instance
        self.__message_id = database_message_instance.id
        self.__anonymous = database_message_instance.anonymous
        self.__author = database_message_instance.author
        self.__message_type = database_message_instance.message_type
        self.__location = database_message_instance.location
        self.__longitude = database_message_instance.longitude
        self.__latitude = database_message_instance.latitude
        self.__created_at_utc = database_message_instance.created_at_utc
        self.__modified_at_utc = database_message_instance.modified_at_utc

    def get_database_message_instance(self):
        return self.__database_message_instance

    def message_id(self):
        return self.__message_id

    @abstractmethod
    def set_title(self, title):
        pass

    @abstractmethod
    def get_title(self):
        pass

    def set_anonymous(self):
        self.__anonymous = True

    def set_onymous(self):
        self.__anonymous = False

    def get_anonymous_status(self):
        return self.__anonymous

    def is_anonymous(self):
        if self.__anonymous is True:
            return True
        return False

    def set_author(self, user):
        self.__author = user

    def get_author(self):
        return self.__author

    def set_message_type(self, message_type):
        self.__message_type = message_type

    def get_message_type(self):
        return self.__message_type

    @abstractmethod
    def set_until_time(self, time):
        pass

    @abstractmethod
    def get_until_time(self):
        pass

    def set_location(self, longitude, latitude):
        self.__longitude = longitude
        self.__latitude = latitude
        point = "POINT(%s %s)" % (float(longitude), float(latitude))
        self.__location = geos.fromstr(point)

    def get_location(self):
        return self.__location

    def set_longitude(self, longitude):
        self.__longitude = longitude
        point = "POINT(%s %s)" % (float(longitude), float(self.__latitude))
        self.__location = geos.fromstr(point)


    def get_longitude(self):
        return self.__longitude

    def set_latitude(self, latitude):
        self.__latitude = latitude
        point = "POINT(%s %s)" % (float(self.__longitude), float(latitude))
        self.__location = geos.fromstr(point)

    def get_latitude(self):
        return self.__latitude

    @abstractmethod
    def increase_total_read_times(self):
        pass

    @abstractmethod
    def get_total_read_times(self):
        pass

    @abstractmethod
    def increase_total_liked_times(self):
        pass

    @abstractmethod
    def get_total_liked_times(self):
        pass

    @abstractmethod
    def increase_total_disliked_times(self):
        pass

    @abstractmethod
    def get_total_disliked_times(self):
        pass

    @abstractmethod
    def set_audio(self, audio):
        pass

    @abstractmethod
    def get_audio(self):
        pass

    @abstractmethod
    def set_image(self, image):
        pass

    @abstractmethod
    def get_image(self):
        pass

    @abstractmethod
    def set_video(self, video):
        pass

    @abstractmethod
    def get_video(self):
        pass

    @abstractmethod
    def set_text(self, text):
        pass

    @abstractmethod
    def get_text(self):
        pass

    @abstractmethod
    def set_commented_message(self, message):
        pass

    @abstractmethod
    def get_commented_message(self):
        pass

    def created_at_utc(self):
        return self.__created_at_utc

    def set_modified_at_utc(self, modified_at_utc):
        self.__modified_at_utc = modified_at_utc

    def get_modified_at_utc(self):
        return self.__modified_at_utc
