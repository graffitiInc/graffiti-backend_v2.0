#!/user/bin/env python
from setuptools import setup

setup(name = 'graffiti-backend',
      version = '2.0',
      description = 'Graffiti backend based on Django framework.',
      author = 'Zhong Yang',
      author_email = 'yzhnasa@gmail.com',
      url = 'https://bitbucket.org/graffitiInc/graffiti-backend_v2.0',
      install_requires=[
        'django>=1.8.5',
        'djangorestframework>=3.2.4',
        'djangorestframework-gis>=0.9.5',
        'django-celery>=3.1.17',
        'Pillow>=3.0.0',
        'enum==0.4.6',
        'psycopg2>=2.6.1'
      ]
)
